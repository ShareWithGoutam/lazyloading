import { Component, OnInit } from '@angular/core';
import { CountriesService } from './countries.service';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'pageone',
  templateUrl: './countries.component.html',
  styles: []
})
export class CountriesComponent implements OnInit {
  private result:any;
  constructor(private service:CountriesService) { }
  ngOnInit() {
    this.service.getcountries().subscribe((posres)=>{
      this.result= posres;
    },(err:HttpErrorResponse)=>{
      if(err.error instanceof Error){
        console.log("client");
      }
      else{
        console.log("server");
      }
    })
  }

}
