import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { lazyRoutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './components/index.component';
import { PageoneComponent } from './components/pageone.component';
import { HttpClientModule } from '@angular/common/http'
@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    PageoneComponent
  ],
  imports: [
    BrowserModule,
    lazyRoutes,HttpClientModule
  ],
  providers: [],
  bootstrap: [IndexComponent]
})
export class AppModule { }
