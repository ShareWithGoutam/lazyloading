import { Routes, RouterModule } from '@angular/router';
import { PageoneComponent } from './components/pageone.component';
import { ModuleWithProviders } from '@angular/core';
import { authguard } from './guards/auth.guard';
export const routes: Routes = [
  {path:"pageone",component:PageoneComponent},
  {path:"lazy",loadChildren:"./countries/countries.module#CountriesModule",
  canLoad:[authguard]}
];
export const lazyRoutes:ModuleWithProviders = RouterModule.forRoot(routes);